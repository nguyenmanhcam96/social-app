import { Component, DoCheck, HostListener } from '@angular/core';
import { Router } from '@angular/router';
import {
  faCog,
  faEdit,
  faSignOutAlt,
  faUser,
  faHome,
  faSignInAlt,
  faUserPlus,
} from '@fortawesome/free-solid-svg-icons';
import { AuthServiceService } from '@app/core/services/auth-service.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements DoCheck {
  title = 'NoobApp';
  userName!: string;
  isLogin: boolean = true;
  faUser = faUser;
  faEdit = faEdit;
  faCog = faCog;
  faSignOutAlt = faSignOutAlt;
  faHome = faHome;
  faSignInAlt = faSignInAlt;
  faUserPlus = faUserPlus;
  
  @HostListener('window:scroll') func1() {
    if (this.authService.isLoggin) {
      this.authService.resetTimer();
    }
  }
  @HostListener('mousemove') func2() {
    if (this.authService.isLoggin) {
      this.authService.resetTimer();
    }
  }
  @HostListener('window:keypress') func3() {
    if (this.authService.isLoggin) {
      this.authService.resetTimer();
    }
  }
  constructor(
    private Router: Router,
    private authService: AuthServiceService
  ) {}
  ngDoCheck(): void {
    this.userName = this.authService.username;
  }
  logOut() {
    this.Router.navigateByUrl('');
    this.authService.logOut();
    this.userName = '';
  }
}
