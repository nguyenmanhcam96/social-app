import { Injectable, EventEmitter } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { IArticle } from '@app/core/models/article-model';
import { TagServiceService } from '@app/core/services/tag-service.service';
import { SettingServiceService } from '@app/core/services/setting-service.service';
import { HttpServiceService } from '@app/core/services/http-service.service';

@Injectable({
  providedIn: 'root',
})
export class ArticleServiceService {
  currentPage: number = 1;
  totalPage: number = 1;
  articleCount: number = 0;
  articleList: IArticle[] = [];

  global: boolean = true;
  favorite: boolean = false;
  username: string = '';

  public changeData$ = new EventEmitter();

  public subject = new BehaviorSubject<any>('');
  constructor(
    private http: HttpServiceService,
    private settingService: SettingServiceService,
    private tagService: TagServiceService
  ) {}
  // get article
  setPage(pageIndex: number) {
    this.currentPage = pageIndex;
    this.getArticleList(this.username);
  }
  getArticleList(username: string) {
    //get global article
    if (this.global) {
      if (this.tagService.currentTag.length != 0) {
        let limit = this.settingService.limit;
        let offset = (this.currentPage - 1) * limit;
        let tag = this.tagService.currentTag;
        //get article by tag
        this.http
          .get(`/api/articles?tag=${tag}&limit=${limit}&offset=${offset}`)
          .subscribe(
            (data) => {
              this.articleList = data.articles;
              this.articleCount = data.articlesCount;
              this.totalPage = Math.ceil(this.articleCount / limit);
              if (this.totalPage === 0) {
                this.totalPage = 1;
              }
              this.changeData$.emit();
            },
            (err) => console.log(err)
          );
      } else {
        let limit = this.settingService.limit;
        let offset = (this.currentPage - 1) * limit;
        //get article without tag
        this.http
          .get(`/api/articles?limit=${limit}&offset=${offset}`)
          .subscribe(
            (data) => {
              this.articleList = data.articles;
              this.articleCount = data.articlesCount;
              this.totalPage = Math.ceil(this.articleCount / limit);
              if (this.totalPage === 0) {
                this.totalPage = 1;
              }
              this.changeData$.emit();
            },
            (err) => console.log(err)
          );
      }
    }
    //get article by user
    else {
      if (!this.favorite) {
        this.username = username;
        let limit = this.settingService.limit;
        let offset = (this.currentPage - 1) * limit;
        this.http
          .get(
            `/api/articles?author=${this.username}&limit=${limit}&offset=${offset}`
          )
          .subscribe(
            (data) => {
              this.articleList = data.articles;
              this.articleCount = data.articlesCount;
              this.totalPage = Math.ceil(this.articleCount / limit);
              if (this.totalPage === 0) {
                this.totalPage = 1;
              }
              this.changeData$.emit();
            },
            (err) => console.log(err)
          );
      } else {
        this.username = username;
        let limit = this.settingService.limit;
        let offset = (this.currentPage - 1) * limit;
        this.http
          .get(
            `/api/articles?favorited=${this.username}&limit=${limit}&offset=${offset}`
          )
          .subscribe(
            (data) => {
              this.articleList = data.articles;
              this.articleCount = data.articlesCount;
              this.totalPage = Math.ceil(this.articleCount / limit);
              if (this.totalPage === 0) {
                this.totalPage = 1;
              }
              this.changeData$.emit();
            },
            (err) => console.log(err)
          );
      }
    }
  }

  getArticle(slug: string | null): Observable<any> {
    return this.http.get(`/api/articles/${slug}`);
  }

  createArticle(body: any): Observable<any> {
    return this.http.post('/api/articles', body);
  }

  updateArticle(slug: string, body: any): Observable<any> {
    return this.http.put(`/api/articles/${slug}`, body);
  }

  deleteArticle(slug: string): Observable<any> {
    return this.http.delete(`/api/articles/${slug}`);
  }

  favoriteArticle(slug: string, body = {}): Observable<any> {
    return this.http.post(`/api/articles/${slug}/favorite`, body);
  }

  unFavoriteArticle(slug: string): Observable<any> {
    return this.http.delete(`/api/articles/${slug}/favorite`);
  }
}
