import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '@env/environment';
@Injectable({
  providedIn: 'root',
})
export class HttpServiceService {
  domain: string = environment.apiUrl;
  constructor(private readonly httpClient: HttpClient) {}

  get(url: string): Observable<any> {
    const newUrl = this.domain + url;
    return this.httpClient.get(newUrl);
  }

  post(url: string, body: any): Observable<any> {
    const newUrl = this.domain + url;
    return this.httpClient.post(newUrl, body);
  }

  put(url: string, body: object): Observable<any> {
    const newUrl = this.domain + url;
    return this.httpClient.put(newUrl, body);
  }

  delete(url: string): Observable<any> {
    const newUrl = this.domain + url;
    return this.httpClient.delete(newUrl);
  }
}
