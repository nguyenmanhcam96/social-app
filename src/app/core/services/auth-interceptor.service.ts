import {
  HttpInterceptor,
  HttpRequest,
  HttpHandler,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AuthServiceService } from './auth-service.service';
@Injectable({
  providedIn: 'root',
})
export class AuthInterceptorService implements HttpInterceptor {
  token: string | null = localStorage.getItem('token');
  constructor(private authService: AuthServiceService) {}
  intercept(req: HttpRequest<any>, next: HttpHandler) {
    this.authService.setToken$.subscribe(() => {
      this.token = localStorage.getItem('token');
      this.authService.resetTimer();
      const authReq = req.clone({
        headers: req.headers.append('Authorization', `Bearer ${this.token}`),
        setHeaders: {
          'Content-type': 'application/json',
        },
      });
      return next.handle(authReq);
    });
    this.authService.resetTimer();
    const authReq = req.clone({
      headers: req.headers.append('Authorization', `Bearer ${this.token}`),
      setHeaders: {
        'Content-type': 'application/json',
      },
    });
    return next.handle(authReq);
  }
}
