import { Injectable } from '@angular/core';
import { HttpServiceService } from '@app/core/services/http-service.service';

@Injectable({
  providedIn: 'root',
})
export class TagServiceService {
  currentTag: string = '';
  constructor(private http: HttpServiceService) {}

  getTags() {
    return this.http.get('/api/tags');
  }
}
