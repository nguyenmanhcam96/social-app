import { HostListener, Injectable, EventEmitter } from '@angular/core';
import { ILogin, IRegister } from '@app/core/models/auth-model';
import { HttpServiceService } from '@app/core/services/http-service.service';

@Injectable({
  providedIn: 'root',
})
export class AuthServiceService {
  username: string = localStorage.getItem('username') || '';
  email: string = localStorage.getItem('email') || '';
  token: string = localStorage.getItem('token') || '';
  isLoggin: boolean = localStorage.getItem('isLoggin') == 'true' ? true : false;
  timer: any;
  setToken$ = new EventEmitter();
  constructor(private http: HttpServiceService) {
    this.setTimer();
  }
  setTimer() {
    if (this.isLoggin)
      this.timer = setTimeout(() => {
        this.logOut();
        console.log('logout');
      }, 600000);
  }
  resetTimer() {
    clearTimeout(this.timer);
    this.setTimer();
  }
  sendRegister(user: IRegister) {
    return this.http.post('/api/users', {
      user: {
        username: user.username,
        email: user.email,
        password: user.password,
      },
    });
  }

  sendLogin(user: ILogin) {
    return this.http.post('/api/users/login', {
      user: { email: user.email, password: user.password },
    });
  }
  setUserLogged(username: string, email: string, token: string) {
    this.username = username;
    this.email = email;
    this.token = token;
    this.isLoggin = true;
    localStorage.setItem('username', username);
    localStorage.setItem('email', email);
    localStorage.setItem('token', token);
    localStorage.setItem('isLoggin', 'true');
    this.setToken$.emit();
    this.setTimer();
  }
  logOut() {
    localStorage.setItem('username', '');
    localStorage.setItem('email', '');
    localStorage.setItem('token', '');
    localStorage.setItem('isLoggin', 'false');
    this.username = '';
    this.email = '';
    this.token = '';
    this.isLoggin = false;
  }
}
