import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class SettingServiceService {
  limit: number = parseInt(localStorage.getItem('limit') || '5');
  constructor() {}
  setLimit(quanti: number) {
    this.limit = quanti;
    localStorage.setItem('limit', `${quanti}`);
  }
}
