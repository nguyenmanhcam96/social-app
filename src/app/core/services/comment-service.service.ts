import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpServiceService } from '@app/core/services/http-service.service';

@Injectable({
  providedIn: 'root',
})
export class CommentServiceService {
  constructor(private http: HttpServiceService) {}

  getCommentList(slug: string): Observable<any> {
    return this.http.get(`/api/articles/${slug}/comments`);
  }

  addComment(slug: string, body: any): Observable<any> {
    return this.http.post(`/api/articles/${slug}/comments`, body);
  }

  deleteComment(slug: string, id: number) {
    return this.http.delete(`/api/articles/${slug}/comments/${id}`);
  }
}
