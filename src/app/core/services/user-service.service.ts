import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { IUser } from '@app/core/models/user-model';
import { HttpServiceService } from '@app/core/services/http-service.service';

@Injectable({
  providedIn: 'root',
})
export class UserServiceService {
  user!: IUser;
  userAuth!: IUser;
  constructor(private http: HttpServiceService) {}

  getUserProfile(user: string): Observable<any> {
    const url = `/api/profiles/${user}`;
    return this.http.get(url);
  }

  updateUserProfile(data: IUser) {
    const url = `/api/user`;
    let userdata = { user: data };
    return this.http.put(url, userdata);
  }

  followingUser(user: string) {
    const url = `/api/profiles/${user}/follow`;
    return this.http.post(url, {});
  }

  unFollowUser(user: string) {
    const url = `/api/profiles/${user}/follow`;
    return this.http.delete(url);
  }

  getFollowings(user: string) {
    const url = `/api/profiles/${user}/following`;
    return this.http.get(url);
  }
}
