import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ArticleListComponent } from '@app/shared/common-components/article-list/article-list.component';
import { CommonComponentRoutingModule } from '@app/shared/common-components/common-component-routing.module';
import { PaginateComponent } from '@app/shared/common-components/paginate-component/paginate.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

@NgModule({
  declarations: [ArticleListComponent, PaginateComponent],
  imports: [CommonModule, CommonComponentRoutingModule, FontAwesomeModule],
  providers: [],
})
export class CommonComponentModule {}
