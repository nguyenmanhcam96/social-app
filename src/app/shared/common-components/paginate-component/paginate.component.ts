import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-paginate',
  templateUrl: './paginate.component.html',
  styleUrls: ['./paginate.component.scss'],
})
export class PaginateComponent implements OnInit {
  @Input() currentPage: number = 1;
  @Input() totalPage: number = 1;
  @Output() pageChange = new EventEmitter();
  constructor() {}

  ngOnInit() {}
  prevClick() {
    this.currentPage--;
    this.pageChange.emit(this.currentPage);
  }
  nextClick() {
    this.currentPage++;
    this.pageChange.emit(this.currentPage);
  }
  pageIndexClick(pageIndex: number) {
    this.currentPage = pageIndex;
    this.pageChange.emit(this.currentPage);
  }
}
