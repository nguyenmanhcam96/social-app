import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { IArticle } from '@app/core/models/article-model';
import { ArticleServiceService } from '@app/core/services/article-service.service';
import { faHeart } from '@fortawesome/free-solid-svg-icons';
@Component({
  selector: 'app-article-list',
  templateUrl: './article-list.component.html',
  styleUrls: ['./article-list.component.scss'],
})
export class ArticleListComponent implements OnInit {
  articleList: IArticle[] = [];
  pageIndex: number = 1;
  totalPage: number = 1;

  faHeart = faHeart;
  constructor(
    private articleService: ArticleServiceService,
    private router: Router
  ) {}

  ngOnInit() {
    this.getArticleList();
  }

  getArticleList() {
    this.articleService.getArticleList(this.articleService.username);
    this.articleService.changeData$.subscribe(
      () => {
        this.articleList = this.articleService.articleList;
        this.pageIndex = this.articleService.currentPage;
        this.totalPage = this.articleService.totalPage;
      },
      (error) => console.log(error)
    );
  }

  pageChangeHandle(index: number) {
    this.articleService.setPage(index);
  }

  handleFavorites(data: IArticle): void {
    if (!localStorage.getItem('username')) {
      alert('You need to login');
      this.router.navigate(['/user/login']);
    } else {
      if (data.favorited === true) {
        this.articleService.unFavoriteArticle(data.slug).subscribe(
          () => this.getArticleList(),
          (error) => console.log(error)
        );
      } else {
        this.articleService.favoriteArticle(data.slug).subscribe(
          () => this.getArticleList(),
          (error) => console.log(error)
        );
      }
    }
  }
}
