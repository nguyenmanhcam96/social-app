import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProfileModuleComponent } from './profile-module.component';
import { ProfileRoutingModule } from './profile-routing.module';
import { ProfileDetailComponent } from './profile-detail/profile-detail.component';
import { UpdateUserComponent } from './update-user/update-user.component';
import { ReactiveFormsModule } from '@angular/forms';
import { FollowingComponent } from './following/following.component';
import { MyFavoriteComponent } from './my-favorite/my-favorite.component';
import { MyPostComponent } from './my-post/my-post.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

@NgModule({
  imports: [
    CommonModule,
    ProfileRoutingModule,
    ReactiveFormsModule,
    FontAwesomeModule,
  ],
  declarations: [
    ProfileModuleComponent,
    ProfileDetailComponent,
    UpdateUserComponent,
    FollowingComponent,
    MyFavoriteComponent,
    MyPostComponent,
  ],
})
export class ProfileModuleModule {}
