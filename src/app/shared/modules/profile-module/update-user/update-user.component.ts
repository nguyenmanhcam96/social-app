import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CanDeactivate, Router } from '@angular/router';
import { AuthServiceService } from '@app/core/services/auth-service.service';
import { CustomvalidationService } from '@app/core/services/customvalidation.service';
import { UserServiceService } from '@app/core/services/user-service.service';
import { CanComponentDeactivate } from '@app/core/guards/canDeactiveEditorArticle-guard.service';
import { Observable } from 'rxjs';
import { ILogin } from '@app/core/models/auth-model';
@Component({
  selector: 'app-update-user',
  templateUrl: './update-user.component.html',
  styleUrls: ['./update-user.component.scss'],
})
export class UpdateUserComponent implements OnInit, CanComponentDeactivate {
  profileForm!: FormGroup;
  // link = '';
  constructor(
    private fb: FormBuilder,
    private router: Router,
    private userService: UserServiceService,
    private customValidator: CustomvalidationService,
    private authService: AuthServiceService
  ) {}
  get f() {
    return this.profileForm?.controls;
  }
  ngOnInit(): void {
    this.createProfileForm();
  }
  canDeactivate(): Observable<boolean> | boolean {
    let conf = confirm(`Do you want exit?
The changes you have made may not be saved. 
    `);
    if (conf == true) {
      return true;
    }
    return false;
  }
  createProfileForm() {
    this.profileForm = this.fb.group(
      {
        image: [`${this.userService.userAuth.image}`, Validators.required],
        username: [
          `${this.userService.userAuth.username}`,
          Validators.required,
        ],
        password: ['', [Validators.required, Validators.minLength(6)]],
        confirmPassword: ['', [Validators.required]],
        bio: [`${this.userService.userAuth.bio}` || '', Validators.required],
      },
      {
        validator: this.customValidator.MatchPassword(
          'password',
          'confirmPassword'
        ),
      }
    );
  }

  onSubmit() {
    this.userService.updateUserProfile(this.profileForm.value).subscribe(
      (res) => {
        this.userService
          .getUserProfile(this.profileForm.value.username)
          .subscribe((data) => {
            this.userService.userAuth = data.profile;
            alert('Update success!!');
            let user: ILogin = {
              email: this.authService.email,
              password: this.profileForm.value.password,
              confirmPassword: this.profileForm.value.confirmPassword,
            };
            this.authService.sendLogin(user).subscribe(
              (res) => {
                if (res.user.token) {
                  this.router.navigateByUrl(
                    `profile/${this.userService.userAuth.username}`
                  );
                  this.authService.setUserLogged(
                    res.user.username,
                    res.user.email,
                    res.user.token
                  );
                }
              },
              (err) => {
                console.log(err);
              }
            );
          });
      },
      (err) => {
        console.log(err);
        alert(err);
      }
    );
  }

  onChangePage() {
    this.router.navigateByUrl(`profile/${this.userService.userAuth.username}`);
  }
}
