import { Component, OnInit } from '@angular/core';
import { ArticleServiceService } from '@app/core/services/article-service.service';

@Component({
  selector: 'app-my-post',
  templateUrl: './my-post.component.html',
  styleUrls: ['./my-post.component.scss'],
})
export class MyPostComponent implements OnInit {
  constructor(private articleService: ArticleServiceService) {}

  ngOnInit() {
    this.articleService.currentPage = 1;
    this.articleService.global = false;
    this.articleService.favorite = false;
    this.articleService.getArticleList(this.articleService.username);
  }
}
