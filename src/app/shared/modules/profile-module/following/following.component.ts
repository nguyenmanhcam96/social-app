import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { IProfile } from '@app/core/models/profile-model';
import { ArticleServiceService } from '@app/core/services/article-service.service';
import { AuthServiceService } from '@app/core/services/auth-service.service';
import { UserServiceService } from '@app/core/services/user-service.service';

@Component({
  selector: 'app-following',
  templateUrl: './following.component.html',
  styleUrls: ['./following.component.scss'],
})
export class FollowingComponent implements OnInit {
  users: string[] = [];

  userParam = '';
  constructor(
    private userService: UserServiceService,
    private authService: AuthServiceService
  ) {}

  ngOnInit() {
    this.getlistFollow();
  }

  getlistFollow() {
    this.userService
      .getFollowings(this.authService.username)
      .subscribe((data) => {
        this.users = data.usersFollowing;
      });
  }
}
