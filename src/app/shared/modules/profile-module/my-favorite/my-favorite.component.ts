import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ArticleServiceService } from '@app/core/services/article-service.service';
import { AuthServiceService } from '@app/core/services/auth-service.service';

@Component({
  selector: 'app-my-favorite',
  templateUrl: './my-favorite.component.html',
  styleUrls: ['./my-favorite.component.scss'],
})
export class MyFavoriteComponent implements OnInit {
  userName = '';
  constructor(
    private articleService: ArticleServiceService,
  ) {}

  ngOnInit() {
    this.articleService.currentPage = 1;
    this.articleService.global = false;
    this.articleService.favorite = true;
    this.articleService.getArticleList(this.articleService.username);
  }
}
