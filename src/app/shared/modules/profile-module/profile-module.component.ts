import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ArticleServiceService } from '../../../core/services/article-service.service';
import { AuthServiceService } from '../../../core/services/auth-service.service';

@Component({
  selector: 'app-profile-module',
  templateUrl: './profile-module.component.html',
  styleUrls: ['./profile-module.component.scss'],
})
export class ProfileModuleComponent implements OnInit {
  userName = '';
  userNameAuth: string = '';
  constructor(
    private activatedRoute: ActivatedRoute,
    private articleService: ArticleServiceService,
    private authService: AuthServiceService
  ) {}

  ngOnInit() {
    this.activatedRoute.params.subscribe((data) => {
      this.userName = data.username;
      this.articleService.username = data.username;
      this.userNameAuth = this.authService.username;
    });
  }
}
