import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CanDeactiveEditorArticleGuardService } from '@app/core/guards/canDeactiveEditorArticle-guard.service';
import { FollowingComponent } from './following/following.component';
import { MyFavoriteComponent } from './my-favorite/my-favorite.component';
import { MyPostComponent } from './my-post/my-post.component';
import { ProfileDetailComponent } from './profile-detail/profile-detail.component';
import { ProfileModuleComponent } from './profile-module.component';
import { UpdateUserComponent } from './update-user/update-user.component';
const routes: Routes = [
  { path: '', redirectTo: 'post' },
  {
    path: '',
    component: ProfileModuleComponent,
    children: [
      {
        path: 'post',
        component: MyPostComponent,
        loadChildren: () =>
          import('../../common-components/common-component.module').then(
            (m) => m.CommonComponentModule
          ),
      },
      {
        path: 'favorite',
        component: MyFavoriteComponent,
        loadChildren: () =>
          import('../../common-components/common-component.module').then(
            (m) => m.CommonComponentModule
          ),
      },
      {
        path: 'following',
        component: FollowingComponent,
      },
    ],
  },
  {
    path: 'profile/:username',
    component: ProfileDetailComponent,
  },
  {
    path: 'update',
    component: UpdateUserComponent,
    canDeactivate: [CanDeactiveEditorArticleGuardService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProfileRoutingModule {}
