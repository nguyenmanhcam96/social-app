import {
  Component,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges,
} from '@angular/core';
import { IProfile } from '@app/core/models/profile-model';
import { UserServiceService } from '@app/core/services/user-service.service';
import { AuthServiceService } from '@app/core/services/auth-service.service';
import { faEdit, faBookmark } from '@fortawesome/free-solid-svg-icons';
@Component({
  selector: 'app-profile-detail',
  templateUrl: './profile-detail.component.html',
  styleUrls: ['./profile-detail.component.scss'],
})
export class ProfileDetailComponent implements OnInit, OnChanges {
  @Input() userName = '';
  userNameAuth: string = '';
  userProFile: IProfile = {
    username: '',
    bio: '',
    image: '',
    following: false,
  };

  isUserlogged: boolean = false;

  faEdit = faEdit;
  faBookMark = faBookmark;
  constructor(
    private userService: UserServiceService,
    private authService: AuthServiceService
  ) {}
  ngOnChanges(): void {
    this.userService.getUserProfile(this.userName).subscribe((data) => {
      this.userProFile = data.profile;
      this.checkUser();
      if (this.isUserlogged) {
        this.userService.userAuth = data.profile;
      }
      this.userService.user = data.profile;
    });
  }

  ngOnInit(): void {
    this.userNameAuth = this.authService.username;
  }

  checkUser() {
    const userlogged = this.authService.username;
    if (this.userProFile.username === userlogged) {
      this.isUserlogged = !this.isUserlogged;
    }
  }

  follow() {
    const user = this.userProFile.username;
    this.userService.followingUser(user).subscribe((data) => {
      this.userProFile.following = data.profile.following;
    });
  }
  unfollow() {
    const user = this.userProFile.username;
    this.userService.unFollowUser(user).subscribe((data) => {
      this.userProFile.following = data.profile.following;
    });
  }
}
