import { Component, Input, OnInit } from '@angular/core';
import { IComment } from '@app/core/models/comment-model';
import { CommentServiceService } from '@app/core/services/comment-service.service';
import { faTrashAlt } from '@fortawesome/free-solid-svg-icons';
@Component({
  selector: 'app-comment-detail',
  templateUrl: './comment-detail.component.html',
  styleUrls: ['./comment-detail.component.scss'],
})
export class CommentDetailComponent implements OnInit {
  @Input() slug!: string;
  commentList!: IComment[];
  authorComment!: string | null;

  faTrashAlt = faTrashAlt;
  constructor(private commentService: CommentServiceService) {}

  ngOnInit() {
    this.authorComment = localStorage.getItem('username');
    this.getCommentList();
  }

  getCommentList() {
    this.commentService.getCommentList(this.slug).subscribe(
      (data) => (this.commentList = data.comments),
      (error) => console.log(error)
    );
  }

  handleDeleteComment(id: number) {
    this.commentService.deleteComment(this.slug, id).subscribe(
      () => this.getCommentList(),
      (error) => console.log(error)
    );
  }
}
