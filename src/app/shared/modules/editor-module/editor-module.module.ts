import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EditorModuleComponent } from './editor-module.component';
import { EditorRoutingModule } from './editor-routing.module';
import { ArticleDetailComponent } from './article-detail/article-detail.component';
import { CommentDetailComponent } from './comment-detail/comment-detail.component';
import { ReactiveFormsModule } from '@angular/forms';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
@NgModule({
  imports: [
    CommonModule,
    EditorRoutingModule,
    ReactiveFormsModule,
    FontAwesomeModule,
  ],
  declarations: [
    EditorModuleComponent,
    ArticleDetailComponent,
    CommentDetailComponent,
  ],
})
export class EditorModuleModule {}
