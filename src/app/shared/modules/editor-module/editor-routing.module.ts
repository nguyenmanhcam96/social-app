import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CanDeactiveEditorArticleGuardService } from '@app/core/guards/canDeactiveEditorArticle-guard.service';
import { EditorModuleComponent } from './editor-module.component';

const routes: Routes = [
  { path: '', component: EditorModuleComponent },
  {
    path: ':slug',
    component: EditorModuleComponent,
    canDeactivate: [CanDeactiveEditorArticleGuardService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EditorRoutingModule {}
