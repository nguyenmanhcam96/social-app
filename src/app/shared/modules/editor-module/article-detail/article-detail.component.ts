import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { map, switchMap } from 'rxjs/operators';
import { IArticle } from '@app/core/models/article-model';
import { ArticleServiceService } from '@app/core/services/article-service.service';
import { CommentServiceService } from '@app/core/services/comment-service.service';
import { UserServiceService } from '@app/core/services/user-service.service';
import {
  faEdit,
  faTrashAlt,
  faBookmark,
  faHeart,
} from '@fortawesome/free-solid-svg-icons';
@Component({
  selector: 'app-article-detail',
  templateUrl: './article-detail.component.html',
  styleUrls: ['./article-detail.component.scss'],
})
export class ArticleDetailComponent implements OnInit {
  article!: IArticle;
  authorArticle!: string | null;
  isFollow!: boolean;

  faEdit = faEdit;
  faTrashAlt = faTrashAlt;
  faBookMark = faBookmark;
  faHeart = faHeart;
  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private articleService: ArticleServiceService,
    private commentService: CommentServiceService,
    private userService: UserServiceService
  ) {}

  ngOnInit() {
    this.activatedRoute.paramMap
      .pipe(
        map((param) => param.get('slug')),
        switchMap((slug) => this.articleService.getArticle(slug))
      )
      .subscribe(
        (data) => {
          this.article = data.article;
          this.isFollow = data.article.author.following;
        },
        (error) => console.log(error)
      );

    this.authorArticle = localStorage.getItem('username');
  }

  handleAddComment(textArea: HTMLTextAreaElement, slug: string) {
    const body = {
      comment: {
        body: textArea.value,
      },
    };
    this.commentService.addComment(slug, body).subscribe();
  }

  handleEditArticle(slug: string) {
    this.router.navigate(['editor', slug]);
  }

  handleDeleteArticle(slug: string) {
    this.articleService.deleteArticle(slug).subscribe(
      () => this.router.navigate(['']),
      (error) => console.log(error)
    );
  }

  handleFollow(article: IArticle, isFollow: boolean) {
    if (!localStorage.getItem('username')) {
      alert('You need to login');
      this.router.navigate(['/user/login']);
    } else {
      if (isFollow === true) {
        this.userService.unFollowUser(article.author.username).subscribe(
          (data) => {
            this.isFollow = data.profile.following;
          },
          (error) => console.log(error)
        );
      } else {
        this.userService.followingUser(article.author.username).subscribe(
          (data) => {
            this.isFollow = data.profile.following;
          },
          (error) => console.log(error)
        );
      }
    }
  }

  handleFavorite(article: IArticle) {
    if (!localStorage.getItem('username')) {
      alert('You need to login');
      this.router.navigate(['/user/login']);
    } else {
      if (article.favorited === true) {
        this.articleService.unFavoriteArticle(article.slug).subscribe(
          (data) => (this.article = data.article),
          (error) => console.log(error)
        );
      } else {
        this.articleService.favoriteArticle(article.slug).subscribe(
          (data) => (this.article = data.article),
          (error) => console.log(error)
        );
      }
    }
  }
}
