import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { map, switchMap } from 'rxjs/operators';
import { IArticle } from '@app/core/models/article-model';
import { ArticleServiceService } from '@app/core/services/article-service.service';
import { CanComponentDeactivate } from '@app/core/guards/canDeactiveEditorArticle-guard.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-editor-module',
  templateUrl: './editor-module.component.html',
  styleUrls: ['./editor-module.component.scss'],
})
export class EditorModuleComponent implements OnInit, CanComponentDeactivate {
  articleForm!: FormGroup;
  submitted = false;
  article!: IArticle;


  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder,
    private articleService: ArticleServiceService
  ) {}

  canDeactivate(): Observable<boolean> | boolean {
    let conf = confirm(`Do you want exit?
The changes you have made may not be saved. 
    `);
    if (conf == true) {
      return true;
    }
    return false;
  }

  ngOnInit() {
    this.articleForm = this.formBuilder.group({
      title: ['', Validators.required],
      description: ['', Validators.required],
      body: ['', Validators.required],
      tagList: [''],
    });

    this.activatedRoute.paramMap
      .pipe(
        map((param) => param.get('slug')),
        switchMap((slug) => this.articleService.getArticle(slug))
      )
      .subscribe(
        (data) => {
          if (data) {
            this.article = data.article;
            this.f['title'].setValue(this.article.title);
            this.f['description'].setValue(this.article.description);
            this.f['body'].setValue(this.article.body);
            this.f['tagList'].setValue(this.article.tagList);
          }
        },
        (error) => console.log(error)
      );
  }

  get f() {
    return this.articleForm.controls;
  }

  handleSubmit() {
    this.submitted = true;
    // stop if form invalid
    if (this.articleForm.invalid) {
      return;
    }
    const body = {
      article: {
        title: this.f['title'].value,
        description: this.f['description'].value,
        body: this.f['body'].value,
        tagList: this.f['tagList'].value.split(','),
      },
    };
    this.articleService.createArticle(body).subscribe(
      () => this.router.navigate(['']),
      (error) => console.log(error)
    );
  }

  handleUpdateArticle(slug: string) {
    const body = {
      article: {
        title: this.f['title'].value,
        description: this.f['description'].value,
        body: this.f['body'].value,
      },
    };
    this.articleService.updateArticle(slug, body).subscribe(
      () => this.router.navigate(['article', slug]),
      (error) => console.log(error)
    );
  }
}
