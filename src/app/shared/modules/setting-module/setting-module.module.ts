import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SettingModuleComponent } from './setting-module.component';
import { SettingRoutingModule } from './setting-routing.module';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [CommonModule, SettingRoutingModule, FormsModule],
  declarations: [SettingModuleComponent],
})
export class SettingModuleModule {}
