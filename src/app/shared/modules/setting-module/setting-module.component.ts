import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SettingServiceService } from '@app/core/services/setting-service.service';

@Component({
  selector: 'app-setting-module',
  templateUrl: './setting-module.component.html',
  styleUrls: ['./setting-module.component.scss'],
})
export class SettingModuleComponent implements OnInit {
  limit: number = this.settingService.limit;
  constructor(
    private settingService: SettingServiceService,
    private router: Router
  ) {}

  ngOnInit() {}
  onClick() {
    this.settingService.setLimit(this.limit);
    this.router.navigateByUrl('');
  }
}
