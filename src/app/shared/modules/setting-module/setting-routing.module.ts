import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SettingModuleComponent } from './setting-module.component';

const routes: Routes = [
  {
    path: '',
    component: SettingModuleComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SettingRoutingModule {}
