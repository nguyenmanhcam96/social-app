import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeModuleComponent } from './home-module.component';
import { HomeRoutingModule } from './home-routing.module';
import { TagListComponent } from './pages/tag-list/tag-list.component';
@NgModule({
  imports: [CommonModule, HomeRoutingModule],
  declarations: [
    HomeModuleComponent,
    TagListComponent
  ],
})
export class HomeModuleModule {}
