import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeModuleComponent } from './home-module.component';
const routes: Routes = [
  {
    path: '',
    component: HomeModuleComponent,
    children: [
      {
        path: '',
        loadChildren: () =>
          import('../../common-components/common-component.module').then(
            (m) => m.CommonComponentModule
          ),
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HomeRoutingModule {}
