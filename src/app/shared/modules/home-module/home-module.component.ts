import { Component, OnInit } from '@angular/core';
import { ArticleServiceService } from '@app/core/services/article-service.service';
import { AuthServiceService } from '@app/core/services/auth-service.service';
import { TagServiceService } from '@app/core/services/tag-service.service';
@Component({
  selector: 'app-home-module',
  templateUrl: './home-module.component.html',
  styleUrls: ['./home-module.component.scss'],
})
export class HomeModuleComponent implements OnInit {
  isLogin: boolean = false;
  isGlobal: boolean = true;
  constructor(
    private authService: AuthServiceService,
    public tagService: TagServiceService,
    private articleService: ArticleServiceService
  ) {}

  ngOnInit() {
    this.articleService.global = true;
    this.articleService.currentPage = 1;
  }
  onSelectGlobal() {
    this.tagService.currentTag = '';
    this.articleService.currentPage = 1;
    this.articleService.getArticleList(this.articleService.username);
  }
}
