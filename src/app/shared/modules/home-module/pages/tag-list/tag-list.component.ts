import { Component, OnInit } from '@angular/core';
import { ArticleServiceService } from '@app/core/services/article-service.service';
import { TagServiceService } from '@app/core/services/tag-service.service';

@Component({
  selector: 'app-tag-list',
  templateUrl: './tag-list.component.html',
  styleUrls: ['./tag-list.component.scss'],
})
export class TagListComponent implements OnInit {
  tagList!: string[];
  constructor(
    public tagService: TagServiceService,
    public articleService: ArticleServiceService
  ) {}

  ngOnInit() {
    this.tagService.getTags().subscribe(
      (data) => (this.tagList = data.tags),
      (error) => console.log(error)
    );
  }

  handleSelectTag(tag: string) {
    this.tagService.currentTag = tag;
    this.articleService.currentPage = 1;
    this.articleService.getArticleList(this.articleService.username);
  }
}
