import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MustMatch } from './confirm-passValidator';
import { AuthServiceService } from '@app/core/services/auth-service.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
})
export class RegisterComponent implements OnInit {
  register!: FormGroup;
  errInfo!: string;
  errEmail!: string;
  errName!: string;
  successRegis: boolean = false;
  constructor(
    private sendData: AuthServiceService,
    private formBuilder: FormBuilder,
    private router: Router
  ) {}

  ngOnInit() {
    this.createForm();
  }
  createForm() {
    this.register = this.formBuilder.group(
      {
        username: ['', Validators.required],
        email: [
          '',
          [
            Validators.required,
            Validators.pattern(
              '[a-z][a-z0-9_.]{2,32}@[a-z0-9]{2,}(.[a-z0-9]{2,4}){1,2}'
            ),
          ],
        ],
        password: ['', [Validators.required, Validators.minLength(6)]],
        confirmPassword: ['', [Validators.required]],
      },
      {
        validator: MustMatch('password', 'confirmPassword'),
      }
    );
  }

  toLogin() {
    this.router.navigateByUrl('/user/login');
  }
  checkValue() {}
  onSubmit() {
    if (this.register.valid) {
      this.sendData.sendRegister(this.register.value).subscribe(
        (res) => {
          if (res.user.token) {
            alert('Register success');
            this.register.reset();
            this.router.navigateByUrl('user/login');
          }
        },
        (err) => {
          this.errName = err.error.errors.username;
          this.errEmail = err.error.errors.email;
        }
      );
    }
  }
}
