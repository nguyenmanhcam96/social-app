import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthServiceService } from '@app/core/services/auth-service.service';
import { UserServiceService } from '@app/core/services/user-service.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  loginForm!: FormGroup;
  errMsg: boolean = false;
  constructor(
    private router: Router,
    private authService: AuthServiceService,
    private formBuilder: FormBuilder,
    private userService: UserServiceService
  ) {}

  ngOnInit() {
    this.createLogin();
  }

  createLogin() {
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required]],
      password: ['', Validators.required],
    });
  }
  switchtoRegister() {
    this.router.navigateByUrl('/user/register');
  }
  onSubmit() {
    if (this.loginForm.valid) {
      this.authService.sendLogin(this.loginForm.value).subscribe(
        (res) => {
          if (res.user.token) {
            this.errMsg = false;
            this.authService.setUserLogged(
              res.user.username,
              res.user.email,
              res.user.token
            );
            this.userService.userAuth = res.user;
            alert('Login Success!');
            this.router.navigateByUrl('');
          }
        },
        (err) => {
          if (err) {
            this.errMsg = true;
            this.loginForm.reset();
          }
        }
      );
    }
  }
}
