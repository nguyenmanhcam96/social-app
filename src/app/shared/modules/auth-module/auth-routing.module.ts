import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NoNeedLoginGuardService } from '@app/core/guards/noNeedLogin-guard.service';
import { AuthModuleComponent } from './auth-module.component';
import { LoginComponent } from './pages/login/login.component';
import { RegisterComponent } from './pages/register/register.component';
const routes: Routes = [
  {
    path: '',
    canActivate:[NoNeedLoginGuardService],
    component: AuthModuleComponent,
    children: [
      {
        path: 'login',
        component: LoginComponent,
      },
      {
        path: 'register',
        component: RegisterComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AuthRoutingModule {}
