import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from './core/guards/auth-guard.service';
import { NoNeedLoginGuardService } from './core/guards/noNeedLogin-guard.service';
import { ArticleDetailComponent } from './shared/modules/editor-module/article-detail/article-detail.component';

const routes: Routes = [
  {
    path: '',
    loadChildren: () =>
      import('./shared/modules/home-module/home-module.module').then(
        (m) => m.HomeModuleModule
      ),
  },
  {
    path: 'user',
    canActivate: [NoNeedLoginGuardService],
    loadChildren: () =>
      import('./shared/modules/auth-module/auth-module.module').then(
        (m) => m.AuthModuleModule
      ),
  },
  {
    path: 'setting',
    canActivate: [AuthGuardService],
    loadChildren: () =>
      import('./shared/modules/setting-module/setting-module.module').then(
        (m) => m.SettingModuleModule
      ),
  },
  {
    path: 'editor',
    loadChildren: () =>
      import('./shared/modules/editor-module/editor-module.module').then(
        (m) => m.EditorModuleModule
      ),
  },
  {
    path: 'profile/:username',
    loadChildren: () =>
      import('./shared/modules/profile-module/profile-module.module').then(
        (m) => m.ProfileModuleModule
      ),
  },
  { path: 'article/:slug', component: ArticleDetailComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
